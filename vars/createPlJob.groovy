
def call(name, stages){

    def dsl="""
pipelineJob('pl-job-1') {
    definition {
        cps {
            sandbox()
            script('''
"""
stages.each{
    dsl+=stageText(it)
}
dsl+="""
            '''.stripIndent())
        }
    }
}
    """
    node {
      jobDsl scriptText: dsl
    }
}
