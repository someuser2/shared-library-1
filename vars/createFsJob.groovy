
def call(name){
    node {
    jobDsl scriptText: """
job("${name}"){
    steps{
      	shell('date')
    }
    publishers {
      groovyPostBuild{
        script('''
			def build = hudson.model.Executor.currentExecutor().currentExecutable
			build.description = build.result
		'''.stripIndent())
      }
    }
}
"""
    }
}

