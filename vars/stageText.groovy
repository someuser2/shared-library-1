
def call(name){

  return """
stage("${name}") {
   def b=build job:"${name}", propagate: false
   currentBuild.description = (currentBuild.description?currentBuild.description+"\\\\n":"")+"${name} "+b.result
   if (!b.result.equals("SUCCESS")) error 'FAIL'
} 
  """
}

